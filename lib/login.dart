import 'package:flutter/material.dart';
import 'package:alimenta/home.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/login_bg.png"),
            fit: BoxFit.contain,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(
                image: AssetImage("assets/images/logo.png"),
                width: 70,
              ),
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/form_bg.png"),
                    fit: BoxFit.contain,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(90.0),
                  child: Column(
                    children:[
                      _buildForm(context),

                      ElevatedButton(
                        //TODO: Auth
                        onPressed: _goToHome,
                        child: Text('Login'),
                        style: ButtonStyle(

                        )
                      ),
                      Text('Developed under the '),
                      Text('Social Hackathon Umbria - CC BY SA'),
                    ]
                  ),
                ),

              )
            ],
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }


  void _goToHome() {
    final navigator = Navigator.of(context);
    final route = MaterialPageRoute(builder: _buildHomePage);
    navigator.pushReplacement(route);
  }
}

Widget _buildHomePage(BuildContext context) => Home();

Widget _buildForm(BuildContext context) => Container(
  child: Form(
    //key: _formKey,
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          width: 200,
          child: TextFormField(
            //controller: _emailController,
            //validator: _validateEmail,
            onSaved: (value) {
              //_email = value;
            },
            decoration: InputDecoration(
              labelText: "Email",
            ),
          ),
        ),
        SizedBox(
          width: 200,
          child: TextFormField(
            //controller: _passwordController,
            //validator: _validatePassword,
            onSaved: (value) {
              //_password = value;
            },
            decoration: InputDecoration(
              labelText: "Password",
            ),
            obscureText: true,
          ),
        ),
      ],
    ),
  ),
);

